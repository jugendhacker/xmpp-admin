package de.jugendhacker.xmppadmin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = getSharedPreferences("credentials", Context.MODE_PRIVATE);
        if (sharedPreferences.getBoolean("xmpp_admin_set", false)){
            Log.i(RegisterUser.class.toString(), "JID: " + sharedPreferences.getString("xmpp_admin_jid", "") + " Password: " + sharedPreferences.getString("xmpp_admin_password", ""));
        }else {
            Intent intent = new Intent(MainActivity.this, AdminLogin.class);
            startActivity(intent);
        }

        CardView cardViewRegisterUser = findViewById(R.id.registerUserCard);
        cardViewRegisterUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegisterUser.class);
                startActivity(intent);
            }
        });

        CardView cardViewAdminLogin = findViewById(R.id.adminAccountCard);
        cardViewAdminLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AdminLogin.class);
                startActivity(intent);
            }
        });
    }
}
