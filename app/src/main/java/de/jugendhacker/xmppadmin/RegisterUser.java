package de.jugendhacker.xmppadmin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.input.InputManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.commands.AdHocCommand;
import org.jivesoftware.smackx.commands.AdHocCommandManager;
import org.jivesoftware.smackx.commands.RemoteCommand;
import org.jivesoftware.smackx.disco.packet.DiscoverItems;
import org.jivesoftware.smackx.xdata.Form;
import org.jivesoftware.smackx.xdata.packet.DataForm;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.io.IOException;
import java.util.List;

public class RegisterUser extends AppCompatActivity {
    private EditText mEditTextNewJID;
    private EditText mEditTextNewPassword;
    private ProgressBar registerUserProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        mEditTextNewJID = findViewById(R.id.editTextNewJID);
        mEditTextNewPassword = findViewById(R.id.editTextNewPassword);
        registerUserProgress = findViewById(R.id.registerProgress);

        Button mCreateButton = findViewById(R.id.createUserButton);
        mCreateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        createAccount();
                    }
                });
                thread.start();
            }
        });
    }

    private void createAccount(){
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                registerUserProgress.setVisibility(View.VISIBLE);
            }
        });

        SharedPreferences sharedPreferences = getSharedPreferences("credentials", Context.MODE_PRIVATE);
        String adminJID = sharedPreferences.getString("xmpp_admin_jid", "");
        String adminPassword = sharedPreferences.getString("xmpp_admin_password", "");

        try {
            XMPPTCPConnection connection = new XMPPTCPConnection(adminJID, adminPassword);
            connection.connect().login();

            AdHocCommandManager manager = AdHocCommandManager.getAddHocCommandsManager(connection);
            final RemoteCommand command = manager.getRemoteCommand(JidCreate.domainBareFrom(adminJID), "http://jabber.org/protocol/admin#add-user");
            command.execute();
            Form answer = command.getForm().createAnswerForm();
            answer.setAnswer("accountjid", mEditTextNewJID.getText().toString());
            answer.setAnswer("password", mEditTextNewPassword.getText().toString());
            answer.setAnswer("password-verify", mEditTextNewPassword.getText().toString());
            try {
                command.complete(answer);
                if (command.isCompleted()){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            registerUserProgress.setVisibility(View.GONE);
                        }
                    });
                    Toast.makeText(this.getApplicationContext(), this.getString(R.string.toast_success), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(RegisterUser.this, MainActivity.class);
                    startActivity(intent);
                }
            } catch (final XMPPException e){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        registerUserProgress.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), getString(R.string.toast_fail) + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

            } catch (final SmackException.NoResponseException e){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), getString(R.string.toast_timeout), Toast.LENGTH_LONG).show();
                    }
                });
            }
        } catch (XmppStringprepException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SmackException e) {
            e.printStackTrace();
        } catch (XMPPException e) {
            e.printStackTrace();
        }
    }
}
