package de.jugendhacker.xmppadmin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class AdminLogin extends AppCompatActivity {
    private EditText mEditTextJID;
    private EditText mEditTextPassword;
    private ConstraintLayout loggedInLayout;
    private LinearLayout loggedOutLayout;
    private Button buttonLogout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        Button mSignInButton = (Button)findViewById(R.id.buttonLogin);
        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogin();
            }
        });

        mEditTextJID = findViewById(R.id.editTextJID);
        mEditTextPassword = findViewById(R.id.editTextPassword);
        loggedInLayout = findViewById(R.id.loggedInLayout);
        loggedOutLayout = findViewById(R.id.loggedOutLayout);
        buttonLogout = findViewById(R.id.buttonAdminLogout);

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getSharedPreferences("credentials", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove("xmpp_admin_jid");
                editor.remove("xmpp_admin_password");
                editor.putBoolean("xmpp_admin_set", false);
                editor.apply();
                Intent intent = new Intent(AdminLogin.this, AdminLogin.class);
                startActivity(intent);
            }
        });

        SharedPreferences sharedPreferences = getSharedPreferences("credentials", Context.MODE_PRIVATE);
        if (sharedPreferences.getBoolean("xmpp_admin_set", false)){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loggedOutLayout.setVisibility(View.GONE);
                    loggedInLayout.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void doLogin(){
        mEditTextJID.setError(null);
        mEditTextPassword.setError(null);

        String JID = mEditTextJID.getText().toString();
        String password = mEditTextPassword.getText().toString();

        //TODO: Build local verification
        SharedPreferences sharedPreferences = getSharedPreferences("credentials", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("xmpp_admin_jid", JID);
        editor.putString("xmpp_admin_password", password);
        editor.putBoolean("xmpp_admin_set", true);
        editor.apply();

        Intent intent = new Intent(AdminLogin.this, MainActivity.class);
        startActivity(intent);
    }
}
